package net.xstream.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

/**
 * Java domain object representing media stream
 *
 * @author Artur Borodziej
 */
@Entity
@Table(name = "stream")
public class Stream {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    /**
     * Stream src
     */
    @Column(name = "src", nullable = false, length = 255)
    private String src;

    /**
     * Stream type
     */
    @Column(name = "type", nullable = false, length = 10)
    private String type;

    /**
     * Media associated with the image
     */
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "stream_media_id", nullable = false)
    private Media media;

    public Stream() {
    }

    public Stream(String src) {
        this.src = src;
    }

    public Stream(String src, String type) {
        this.src = src;
        this.type = type;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSrc() {
        return src;
    }

    public void setSrc(String src) {
        this.src = src;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Media getMedia() {
        return media;
    }

    public void setMedia(Media media) {
        this.media = media;
    }

    @Override
    public String toString() {
        return "Stream{" +
                "id=" + id +
                ", src='" + src + '\'' +
                ", type='" + type + '\'' +
                ", media=" + media +
                '}';
    }
}
