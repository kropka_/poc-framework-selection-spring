package net.xstream.entity;

import org.springframework.data.rest.core.annotation.RestResource;

import javax.persistence.*;
import java.util.Set;

/**
 * Java domain object representing media
 *
 * @author Artur Borodziej
 */
@Entity
@Table(name = "media")
public class Media {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    /**
     * External media id used content provider
     */
    @Column(name = "external_id", nullable = true, length = 255, unique = true)
    private String externalId;

    /**
     * Media title
     */
    @Column(name = "title", nullable = false, length = 255)
    private String title;

    /**
     * Media description
     */
    @Column(name = "description", nullable = true, length = 255)
    private String description;

    /**
     * Images associated with given media
     */
    @OneToMany(mappedBy = "media", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private Set<Image> images;

    /**
     * Streams associated with given media
     */
    @OneToMany(mappedBy = "media", cascade = CascadeType.ALL)
    private Set<Stream> streams;

    public Media() {
    }

    public Media(String title) {
        this.title = title;
    }

    public Media(String title, String description) {
        this.title = title;
        this.description = description;
    }

    public Media(String title, String description, String externalId) {
        this.title = title;
        this.description = description;
        this.externalId = externalId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<Image> getImages() {
        return images;
    }

    public void setImages(Set<Image> images) {
        for (Image image : images) {
            image.setMedia(this);
        }
        this.images = images;
    }

    public Set<Stream> getStreams() {
        return streams;
    }

    public void setStreams(Set<Stream> streams) {
        for (Stream stream : streams) {
            stream.setMedia(this);
        }
        this.streams = streams;
    }

    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    @Override
    public String toString() {
        return "Media{" +
                "id=" + id +
                ", externalId='" + externalId + '\'' +
                ", title='" + title + '\'' +
                ", description='" + description +
                '}';
    }
}
