package net.xstream.config;

import net.xstream.entity.Image;
import net.xstream.entity.Media;
import net.xstream.entity.Stream;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestMvcConfiguration;

/**
 * Configuration for rest repositories
 *
 * @author Artur Borodziej
 */
@Configuration
public class PocFrameworkSelectionSpringRepositoryRestMvcConfiguration extends RepositoryRestMvcConfiguration {
    @Override
    protected void configureRepositoryRestConfiguration(RepositoryRestConfiguration config) {
        config.exposeIdsFor(Media.class);
        config.exposeIdsFor(Image.class);
        config.exposeIdsFor(Stream.class);
    }
}
