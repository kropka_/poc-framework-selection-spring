package net.xstream.djament_import.queue;

import net.xstream.config.RabbitMQConfig;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by artur.borodziej on 17.06.15.
 */
@Service
public class Notifier {

    private RabbitTemplate rabbitTemplate;

    @Autowired
    public Notifier(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

    public void sendNotify(String message) {
        rabbitTemplate.convertAndSend(RabbitMQConfig.queueName, message);
    }
}
