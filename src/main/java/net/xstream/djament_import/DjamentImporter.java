package net.xstream.djament_import;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import net.xstream.entity.Image;
import net.xstream.entity.Media;
import net.xstream.entity.Stream;
import net.xstream.repository.MediaRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashSet;
import java.util.Iterator;

import static javax.xml.bind.DatatypeConverter.printBase64Binary;

/**
 * Created by artur.borodziej on 16.06.15.
 */
@Service
public class DjamentImporter {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private MediaRepository mediaRepository;

    @Autowired
    public DjamentImporter(MediaRepository mediaRepository) {
        this.mediaRepository = mediaRepository;
    }

    public void importFromUrl(String djamentUrl) {
        logger.info("Importing data from {}", djamentUrl);
        JsonNode node = null;
        try {
            URL url = new URL(djamentUrl);
            URLConnection connection = url.openConnection();
            String credentials = "test:test";
            String basicAuth = String.format("Basic %s", printBase64Binary(credentials.getBytes()));
            connection.setRequestProperty ("Authorization", basicAuth);
            node = new ObjectMapper().readTree(connection.getInputStream());
            if (node.has("list")) {
                Iterator<JsonNode> mediaNodes = node.get("list").elements();
                while (mediaNodes.hasNext()) {
                    JsonNode mediaNode = mediaNodes.next();
                    logger.info(mediaNode.toString());
                    importMedia(mediaNode);
                }
            } else {
                importMedia(node);
            };
        } catch (IOException e) {
            logger.error("Could not read {}", djamentUrl);
        }
        logger.info("Import finished");
    }

    private void importMedia(JsonNode node) {
        String externalId = node.get("external_id").asText();
        Media media = (Media) mediaRepository.findOneByExternalId(externalId);
        if (media != null) {
            logger.warn("Media with media id {} already imported", externalId);
            return;
        }
        media = new Media(
                node.get("titles").get("default").asText(),
                node.get("descriptions").get("default").asText(),
                externalId);
        logger.info("Creating {}", media);

        Iterator<JsonNode> imageNodes = node.get("images").elements();
        HashSet<Image> images = new HashSet<>();
        while (imageNodes.hasNext()) {
            JsonNode formatNode = imageNodes.next().get("formats").get("original_size");
            Image image = new Image(formatNode.get("src").asText(),
                    formatNode.get("width").asInt(),
                    formatNode.get("height").asInt());
            images.add(image);
            logger.info("Creating {}", image);
        }
        Iterator<JsonNode> streamNodes = node.get("streams").elements();
        HashSet<Stream> streams = new HashSet<>();
        while (streamNodes.hasNext()) {
            JsonNode streamNode = streamNodes.next();
            if (streamNode.isArray()) {
                streamNode = streamNode.get(0);
                Stream stream = new Stream(
                        streamNode.get("src").asText(),
                        streamNode.get("type").asText());
                streams.add(stream);
                logger.info("Creating {}", stream);
            }
        }
        media.setImages(images);
        media.setStreams(streams);

        mediaRepository.save(media);
        logger.info("Media saved successfully");
    }
}
