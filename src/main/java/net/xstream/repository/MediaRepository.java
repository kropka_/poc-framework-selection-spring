package net.xstream.repository;

import net.xstream.entity.Media;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * Repository class for <code>Media<code/>s domain objects
 *
 * @author Artur Borodziej
 */
public interface MediaRepository extends PagingAndSortingRepository<Media, Long> {

//    @Query("SELECT m FROM Media WHERE")
//    List<Media> findAllByParameters();

    Media findOneByExternalId(String externalId);
}
