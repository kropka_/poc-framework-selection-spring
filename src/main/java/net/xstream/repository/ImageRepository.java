package net.xstream.repository;

import net.xstream.entity.Image;
import org.springframework.data.repository.CrudRepository;

/**
 * Repository class for <code>Image<code/>s domain objects
 *
 * @author Artur Borodziej
 */
public interface ImageRepository extends CrudRepository<Image, Long> {
}
