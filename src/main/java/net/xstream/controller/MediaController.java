package net.xstream.controller;

import net.xstream.dto.MediaListResponse;
import net.xstream.entity.Media;
import net.xstream.repository.MediaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * Created by artur.borodziej on 17.06.15.
 */
@RestController
@RequestMapping("/media")
public class MediaController {

    private final MediaRepository repository;

    @Autowired
    public MediaController(MediaRepository repository) {
        this.repository = repository;
    }

    @RequestMapping(method = RequestMethod.POST)
    public void create(@RequestBody @Valid Media media) {
        repository.save(media);
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public MediaListResponse read(@RequestParam(required = false, defaultValue = "10") int limit,
                                  @RequestParam(required = false, defaultValue = "0") int offset,
                                  @RequestParam(required = false, defaultValue = "id") String sort,
                                  @RequestParam(required = false, defaultValue = "asc") String order) {
        List<Media> media = (List<Media>) repository.findAll();
        MediaListResponse response = new MediaListResponse(media, limit, offset, repository.count());

        return response;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Media findOne(@PathVariable Long id) {
        return repository.findOne(id);
    }

/*    @RequestMapping( value = "/{id}", method = RequestMethod.PUT)
    public void update(@PathVariable Long id, @RequestBody @Valid Media updated) {
        Media media = repository.findOne(id);

//        repository.save(media);
    }*/

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable Long id) {
        Media media = repository.findOne(id);
        repository.delete(media);
    }
}
