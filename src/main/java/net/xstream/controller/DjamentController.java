package net.xstream.controller;

import net.xstream.djament_import.queue.Notifier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by artur.borodziej on 16.06.15.
 */
@RestController
@RequestMapping("/djament")
public class DjamentController {

    private Notifier notifier;

    @Autowired
    public DjamentController(Notifier notifier) {
        this.notifier = notifier;
    }

    @RequestMapping(value = "/import", method = RequestMethod.POST)
    public void importDjament(@RequestParam(value = "url") String url) {
        notifier.sendNotify(url);
    }
}
