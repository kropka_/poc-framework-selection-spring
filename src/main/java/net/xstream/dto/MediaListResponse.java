package net.xstream.dto;

import net.xstream.entity.Media;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by artur.borodziej on 17.06.15.
 */
public class MediaListResponse {

    private List<ListedMedia> media;

    private int limit;

    private int offset;

    private long total;

    private Map<String, Map> links;

    public MediaListResponse() {
    }

    public MediaListResponse(List<Media> media, int limit, int offset, long total) {
        ArrayList<ListedMedia> listedMedia = new ArrayList<>();
        for (Media medium : media) {
            listedMedia.add(new ListedMedia(medium));
        }
        this.media = listedMedia;
        this.limit = limit;
        this.offset = offset;
        this.total = total;
    }

    public List<ListedMedia> getMedia() {
        return media;
    }

    public int getLimit() {
        return limit;
    }

    public int getOffset() {
        return offset;
    }

    public long getTotal() {
        return total;
    }

    public Map<String, Map> getLinks() {
        return links;
    }
}
