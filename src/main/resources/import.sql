SET FOREIGN_KEY_CHECKS = 0;

-- CLEAN UP DATABASE
TRUNCATE `image`;
TRUNCATE `media`;

-- LOAD MEDIA
INSERT INTO `media` (`description`, `title`) VALUES ('FIXTURE MEDIA TITLE 1', 'FIXTURE MEDIA DESCRIPTION 1');
INSERT INTO `media` (`description`, `title`) VALUES ('FIXTURE MEDIA TITLE 2', 'FIXTURE MEDIA DESCRIPTION 2');
INSERT INTO `media` (`description`, `title`) VALUES ('FIXTURE MEDIA TITLE 3', 'FIXTURE MEDIA DESCRIPTION 3');
INSERT INTO `media` (`description`, `title`) VALUES ('FIXTURE MEDIA TITLE 4', 'FIXTURE MEDIA DESCRIPTION 4');

-- LOAD IMAGES
INSERT INTO `image` (`height`, `src`, `width`, `image_media_id`) VALUES (83, 'http://www.dotkam.com/wp-content/uploads/2009/10/spring-source-logo.gif', 5, 1);
INSERT INTO `image` (`height`, `src`, `width`, `image_media_id`) VALUES (220, 'http://forum.spring.io/core/image.php?userid=38939&dateline=1378757183', 220, 1);
INSERT INTO `image` (`height`, `src`, `width`, `image_media_id`) VALUES (300, 'http://incdn1.b0.upaiyun.com/2012/11/springsource_logo.png', 300, 2);
INSERT INTO `image` (`height`, `src`, `width`, `image_media_id`) VALUES (270, 'http://lkrnac.net/wp-stuff/uploads/2014/12/spring-framework-logo-604x270.png', 604, 2);
INSERT INTO `image` (`height`, `src`, `width`, `image_media_id`) VALUES (151, 'http://2.bp.blogspot.com/-4szSvKyc9x4/ThsY0GuACwI/AAAAAAAAABI/eptCgIwbTpw/s1600/Logo_Spring_412x151.png', 412, 3);

-- LOAD STREAMS

SET FOREIGN_KEY_CHECKS = 1;